# Rails 5 + Create React App + Tailwind CSS Stack Template

Template to get a Rails 5 API only backend with a React frontend using Tailwind CSS

## Requirements

- yarn
- create-react-app
- Rails 5.1.x
- PostgreSQL

## Installation

Generate a new Rails application by passing in the options below:

```
rails new blog \
-d postgresql \
-T \
-m https://gitlab.com/eddieonrails/rails-cra-tailwindcss/raw/master/template.rb
```

Optional: Make this template to create new Rails application by creating a `~/.railsrc` file and pasting the following code:

```
-d postgresql
-T
-m https://gitlab.com/eddieonrails/rails-cra-tailwindcss/raw/master/template.rb
```

## Deploy

Start the server

```
cd blog

rake start
```

Use React

```
blog/client
```

Custom styles for Tailwind

```
blog/client/src/styles/custom.css
```
