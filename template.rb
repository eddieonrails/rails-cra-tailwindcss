
def add_foreman
  gem 'foreman'
end

def add_procfile_dev
  File.open("Procfile.dev", "w") do |line|
    line.puts "web:  sh -c 'PORT=3000 yarn --cwd client start'" + "\r"
    line.puts "api: bundle exec rails s -p 3001"
  end
end

def add_procfile_prod
  File.open("Procfile", "w") do |line|
    line.puts "web: bundle exec rails s"
  end
end

def add_rake_start_task
  File.open("lib/tasks/start.rake", "w") do |line|
    line.puts "namespace :start do"
    line.puts " task :development do"
    line.puts "   exec 'heroku local -f Procfile.dev'"
    line.puts " end"
    line.puts "end"
    line.puts ""
    line.puts "desc 'Start development server'"
    line.puts "task start: 'start:development'"
  end
end

def add_package_json
  File.open("package.json", "w") do |line|
    line.puts '{'
    line.puts '  "name": "rails-react-cra-tailwind",'
    line.puts '  "engines": {'
    line.puts '    "node": "8.11.3",'
    line.puts '    "yarn": "1.7.0"'
    line.puts '  },'
    line.puts '  "scripts": {'
    line.puts '    "build": "yarn --cwd client install && yarn --cwd client build",'
    line.puts '    "deploy": "cp -a client/build/. public/",'
    line.puts '    "heroku-postbuild": "yarn build && yarn deploy"'
    line.puts '  }'
    line.puts '}'
  end
end

def run_create_react_app
  system 'create-react-app client'
end  

def ammend_cra_package_json
  file = File.read("client/package.json")
  scripts = ' "scripts": {
      "build:css": "postcss src/styles/custom.css -o src/index.css",
      "watch:css": "postcss src/styles/custom.css -o src/index.css -w",
      "start": "yarn watch:css & react-scripts start",
      "build": "yarn build:css && react-scripts build",
      "test": "react-scripts test --env=jsdom",
      "eject": "react-scripts eject"
    }'
  replace = file.gsub(/"scripts": {([\S\s]*?)}/, scripts)
  File.write("client/package.json", replace)
end

def add_proxy
  file = File.read("client/package.json")
  devs = file[/"devDependencies": {([\S\s]*?)}/]
  devs << ",\n"
  devs << '  "proxy": "http://localhost:3001"'
  replace = file.gsub(/"devDependencies\": {([\S\s]*?)}/, devs)
  File.write("client/package.json", replace)
end

def add_dev_dependencies
  system 'cd client && yarn add autoprefixer postcss-cli tailwindcss -D'
  system 'cd client && yarn add eslint eslint-config-airbnb eslint-plugin-import eslint-plugin-jsx-a11y eslint-plugin-react -D'
end

def add_eslint_rc
  File.open('client/.eslintrc', "w") do |line|
    line.puts '{'
    line.puts '  "extends": "airbnb",'
    line.puts '  "rules": {'
    line.puts '    "react/jsx-filename-extension": [1, { "extensions": [".js", ".jsx"] }]'
    line.puts '  }'
    line.puts '}'
  end
end

def add_postcss_config_js
  File.open("client/postcss.config.js", "w") do |line|
    line.puts "const tailwindcss = require('tailwindcss');"
    line.puts "module.exports = {"
    line.puts "  plugins: [tailwindcss('./tailwind.config.js'), require('autoprefixer')]"
    line.puts "};"
  end
end

def init_tailwindcss
  system 'cd client && node_modules/.bin/tailwind init tailwind.config.js'
end

def add_custom_styles
  Dir.mkdir 'client/src/styles'
  File.open('client/src/styles/custom.css', "w") do |line|
    line.puts "@tailwind preflight;"
    line.puts "@tailwind utilities;"
    line.puts ""
    line.puts "/* Your custom CSS here */"
  end
end

add_foreman
add_procfile_dev
add_procfile_prod
add_rake_start_task
add_package_json
run_create_react_app
ammend_cra_package_json
add_postcss_config_js
add_dev_dependencies
add_proxy
add_eslint_rc
init_tailwindcss
add_custom_styles